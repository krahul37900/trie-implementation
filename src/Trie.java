import java.util.LinkedList;
import java.util.List;


public class Trie {

	TrieNode root;
	
	/**
	 * Construct a trie with root
	 * '0' representing that its root.
	 */
	public Trie() 
	{
		this.root = new TrieNode('0');
	}
	
	
	/**
	 * Adds a string to the trie if it does not exist.
	 * @param s
	 */
	public void add(String s)
	{
		this.root.add(s);
	}
	
	/**
	 * Display contents of the trie
	 */
	public void displayTrie()
	{
		this.root.displayTrie();
	}

	/**
	 * Return all the possibilities for the given prefix string from the trie
	 * @param string
	 */
	public List<String> autoComplete(String prefix) {
		 List<String> res =  this.root.autoComplete(prefix);
		 List<String> res1 = new LinkedList<String>();
		 if (null != res) 
		 {
			 for(String s : res)
			 {
				 s = prefix + s;
				 res1.add(s);			 
			 }
		}
		 return res1;
	}
	
}
