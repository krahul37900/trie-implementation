import java.util.List;



public class Client {

	public static void main(String[] args) {
		Trie t1 = new Trie();
//		t1.add("abcd");
//		t1.add("abc");
//		t1.add("ab");
//		t1.add("ae");
//		t1.add("af");
//		t1.add("f");
		t1.add("how");
		t1.add("hey");
		//t1.displayTrie();
		
		List<String> res = t1.autoComplete("m");
		if(res.isEmpty())
			System.out.println("Given Prefix does not exist in the trie");
		else
		{
			for(String s : res)
			{
				System.out.println(s);
			}
		}
		
	}

}
