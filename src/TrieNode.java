import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


public class TrieNode {

	char letter;
	boolean endWord;
	TrieNode[] children;
	
	/**
	 * Construct a Trie Node 
	 * @param letter
	 */
	public TrieNode(char letter)
	{
		this.letter = letter;
		this.endWord = false;
		this.children = new TrieNode[26];
	}
	
	/**
	 * Add string to the trie
	 * @param s
	 */
	public void add(String s)
	{
		if(s.isEmpty())
		{
			this.endWord = true;
			return;
		}
		int index = s.charAt(0) - 'a';
		if (null == this.children[index])
		{
			this.children[index] = new TrieNode(s.charAt(0));
		}
		this.children[index].add(s.substring(1));
	}
	
	/**
	 * Display the contents of the Trie.
	 */
	public void displayTrie()
	{
		System.out.println("ROOT = " + this.letter);
		
		Queue<TrieNode> queue = new LinkedList<TrieNode>();
		queue.add(this);
		
		while(!queue.isEmpty()) 
		{
			TrieNode node = queue.remove();
			for(int i = 0; i < 26; ++i)
			{
				if(null != node.children[i])
				{
					System.out.print(node.children[i].letter);
					queue.add(node.children[i]);
				}
			}
			System.out.println();
		}
	}

	/**
	 * Returns the list of all possible strings following the given string.
	 * @param string
	 * @return
	 */
	public List<String> autoComplete(String string) {
		if(string.isEmpty())
		{
			return getStrings(this, string);
		}
		int c = string.charAt(0) - 'a';
		if(null != this.children[c])
		{
			return this.children[c].autoComplete(string.substring(1));
		}
		return null;
	}

	/* Do a DFS and return all branches characters concatenated as strings.
	 *
	 */
	private List<String> getStrings(TrieNode trieNode, String prefix) {
		List<String> res = new LinkedList<String>();
		dfs(trieNode, res, "");
		return res;
	}

	private void dfs(TrieNode trieNode, List<String> res, String s) {
		
		
		for(int i = 0; i < 26; ++i){
			if( null != trieNode.children[i])
				dfs(trieNode.children[i], res, s + trieNode.children[i].letter);
		}
		if(trieNode.endWord)
			res.add(s);
	}
}
